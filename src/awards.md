---
title: Awards
---
![](https://static.igem.wiki/teams/4203/wiki/awards.jpg)
## Awards Education

We held activities to introduce synthetic biology to the public. We set an exhibition in a showroom which is about biological technologies in China Science and Technology Museum. We not only designed leaflets and playbill to introduce knowledge of some basic technologies in biology and what synthetic biology is to the public, but also brought some badges and key buckles with some cute pictures of microorganism made by our group mates, trying to arouse children's interests and raise their awareness of microorganism and synthetic biology. In addition, we held a lecture in Beihai Park. We have configured media that allow visitors to experience the process of cultivating microorganisms and come into close contact with them, thus understanding the importance of microbial and synthetic biology.
![Students of Beijing No. 5 High School give a lecture at the Science and Technology Museum](https://static.igem.wiki/teams/4203/wiki/science-and-technology-museum7.jpg)
>Students of Beijing No. 5 High School give a lecture at the Science and Technology Museum

## Award Entrepreneurship

Due to the influence of epidemic factors, our project has not been implemented to the stage of commercial operation, so we have not applied for provisional patent. If there is an opportunity, we plan to apply this bacteria in cancer treatment and hearing AIDS, and make corresponding contributions in the field of medicine and physics. If the pandemic eases, we plan to promote and raise funds on campus and outside, find appropriate investors to pitch the idea, and build and ship it with relevant companies.

## Diversity and Inclusion

Methods of publicity: to give a speech to visitors at the science and Technology Museum. Contents learned: When popularizing biological knowledge, iGEM competition and the overview of this group of experiments for the public, we can use some languages that the public (non-professionals in the field of biological knowledge) can understand to explain to the public. For example, we can compare fungi to Lego, and genetic engineering to disassemble parts and install specific parts to drive specific functions. During this period, we learned the direct connection between biological knowledge and science and technology, which laid a good foundation for our project prospects. In addition, we learned how to develop the program into other aspects (such as medicine and physics) in the process of learning, which reflects the diversity and inclusiveness of the program.

## Sustainable Development Goals

::: tip Sustainable Development Goal 3 - to enable all people of all ages to lead healthy lives and improve their well-being

In line with Sustainable Development Goal 3 - to enable all people of all ages to lead healthy lives and improve their well-being. The BJWZ-China project is dedicated to the research and development of sound, and our project soon led to the first development direction: to manipulate cells to sense cancer using sound. Hearing is the ability of animals to perceive sound and is the process of converting mechanical energy into electromagnetic signals. However, the mechanism is not fully understood, and the existence of similar sensory systems in microorganisms has not been studied. BJWZ-china is ready to try to be a pioneer. We intend to use the "Nan-IAV pattern", an ion channel that forms the mechanical transduction of hearing in Drosophila, and introduce it into Saccharomyces cerevisiae to construct an auditory module to study this living phenomenon, as the first round of the cancer-sensing project. The follow-up study of sound transduction in microorganisms was preliminally explored. So that more patients can be more timely treatment in the early stage, improve the efficiency of treatment.

In addition, the project of the BJWZ-China Cooperation Team and the Beijing Normal University team is for the research of cannabis vaccine. The project aims to treat some people who are forced to use cannabis and provide first aid for their addiction caused by THC. Firstly, the bacteria were defined in the intestinal tract and expressed with the bacteria, and then the bacteria were used to express three enzymes to carry out inactive degradation of THC in vivo, and finally express phycocyanin. If the degradation is successful, phycocyanin will give the stool a blue color to verify that it has been successfully transformed in the body. This process can effectively inhibit cannabis addiction and thus have a therapeutic effect.
:::

::: tip Sustainable Goal 17 - Strengthen means of implementation and revitalize the global partnership for sustainable development

In line with Sustainable Goal 17 - Strengthen means of implementation and revitalize the global partnership for sustainable development.The World Health Organization (WHO) predicts that cancer will become the leading killer of human beings in the 21st century, so cancer control has become a global focus of health strategy. This requires a multi-dimensional approach from national strategies, multi-party collaboration, early screening promotion, technology improvement, knowledge dissemination and habit improvement to have a hope of improving the ability to fight cancer.

The experiment of BJWZ-China is only the most preliminary contribution to the awareness of cancer, because the treatment and prevention of cancer is a very important and difficult process, and we hope to lay the groundwork for the first step for future teams. We hope that future technology or new results will assist our transformation of Saccharomyces cerevisiae and provide new ways to detect cancer. On this basis, if it can effectively provide ideas for the medical field, it will greatly enhance the fight against common diseases of mankind, so as to better stimulate the vitality of global partnership.

In the world, more and more countries are legalizing marijuana, but there are also some regions that list marijuana as a drug. For some students, tourists, anti-drug police, etc., who are forced to use marijuana or accidentally eat marijuana, the project developed by the team of Beijing Normal University can inhibit the effect.Vaccination, the most cost-effective public health intervention for the prevention and control of infectious diseases, is an effective means of reducing disease and health costs for family members and the disease, and global vaccination coverage (the proportion of children in which vaccines are recommended globally) has remained stable over the past few years, which can stimulate the vitality of global partnerships.
:::
