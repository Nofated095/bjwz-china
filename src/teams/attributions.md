---
title: Attributions
---
![](https://static.igem.wiki/teams/4203/wiki/attributionnew.jpg)

![](https://static.igem.wiki/teams/4203/wiki/rreeeee.jpeg)

## Propose the application of sound perception yeast:

Thank you to everyone who helped the team achieve success. First of all, we would like to thank the school for providing us with the opportunity to participate in the iGEM competition and the financial support. It is impossible for a high school team to complete this project without the support of the school. Secondly, I would like to thank our tutor (Zhang Hongjun) and team leader (Xiang Biyun). It was Zhang who initially proposed the intention of participating in the competition and recruited interested students to form the student team, which also served as a bridge between the student team and the school. On the other hand, Teacher Xiang led the student team to conduct experimental operation training and explain basic knowledge. During the tutoring period, she made strict demands on us and always reminded us to pay attention to laboratory safety. In order to ensure that the teacher would be present when we did the experiment, she even stayed at the school overnight. Then we would like to thank the companies that helped us synthesize the genes and provide the equipment during the experiment (I can add names to this section, but I don't know what they are), and MathWorks for their software support. Finally, we would like to thank our parents for their understanding and support.

In addition, the laboratory is provided by Beijing No.5 High School and the Wiki guidance is provided by Niu Zishan's relatives Nie Zhijun and Dong Xin.Presentation coaching is provided by BUN-China.