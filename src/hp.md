---
title: Human Practice
---
![](https://static.igem.wiki/teams/4203/wiki/humanpractice.jpg)

## Document creative ways to solve problems

It is a creative way for us to solve problems by drawing inspiration from existing literature to ask questions by analogy and making innovations in existing solutions.
Initially, our project was devoted to the study and deepening of sound, which came from a report we saw about cell acoustics at the beginning of the project. If all cells were indeed beating like heart cells, would the vibration be enough to produce a detectable sound? Can we make our bacteria sense this sound? Is there any use for such research? We followed the cellular acoustic clues in anticipation.

What we found in the data was this: "Each cell produces its own unique sound."
"Motor proteins are linked to the cytoskeleton [responsible for maintaining the structural integrity and shape of the cell], which in turn is linked to the cell membrane. Therefore, the movement of motor proteins is thought to be transmitted through the cytoskeleton to the cell membrane, causing it to oscillate." "The uniform movement of motor proteins within the cell is thought to be behind the cellular sound," and these data support our idea to some extent.
Later, we were inspired by Drosophila melanogaster. The ion channel "Nan-IAV mode", which constitutes the mechanical transduction of hearing in Drosophila melanogaster, was expressed in Saccharomyces cerevisiae, and the downstream signal was amplified to the cytoskeleton by Tether(Suntag-SCFV), so as to realize the transition from mechanical stimulation to physiological activity. Observe the response of yeast cells to sound waves. Our experiments in Drosophila are further revealed, which fully reflects our ability to exploit the existing literature, extract its ion channels and express them in Saccharomyces cerevisiae, which shows our innovative ability
This way has been improved through gradual experiments. We have expanded the ways and methods of learning, not only learning from literature, but also obtaining corresponding inspiration in life. The principle of hearing, for example, is best demonstrated by the discovery of the animal kingdom. We have learned to associate knowledge with nature, reflecting the evolution of the process.
It also includes learning from the advice of others or growing in discussions with other groups. We both broadened our horizons, which greatly reduced our burdens and overcame many problems in the course of the experiment. We recognize that inspiration comes from many different sources and that it is through continuous integration that the best results are achieved.

## Diversity and Inclusion

We disseminate knowledge of synthetic biology by giving a speech to visitors at the Science and Technology Museum.When popularizing biological knowledge,iGEM competition and the overview of this group of experiments for the public, we can use some languages that the public，which are non-professionals in the field of biological knowledge, can understand to explain to the public. For example, we can compare fungi to Lego, and genetic engineering to disassemble parts and install specific parts to drive specific functions.
![Students of Beijing No. 5 High School give speeches at the Science and Technology Museum](https://static.igem.wiki/teams/4203/wiki/science-and-technology-museum3.jpg)
>Students of Beijing No. 5 High School give speeches at the Science and Technology Museum

During this period, we learned the direct connection between biological knowledge and science and technology, which laid a good foundation for our project prospects. In addition, we learned how to develop the program into other aspects (such as medicine and physics) in the process of learning, which reflects the diversity and inclusiveness of the program.