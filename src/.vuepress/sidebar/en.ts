import { sidebar } from "vuepress-theme-hope";

export const enSidebar = sidebar({
  "/": [
    "",
    {
      text: "Teams",
      prefix: "teams/",
      link: "teams/",
      children: "structure",
    },
    {
      text: "Projects",
      prefix: "projects/",
      link: "projects/",
      children: "structure",
    },
    {
      text: "Parts",
      icon: "book",
      prefix: "parts/",
      children: "structure",
    },
    "awards",
    "hp",
    "safety",
  ],
});
