import { defineUserConfig } from "@vuepress/cli";
import theme from "./theme.js";

const base = <"/" | `/${string}/`>process.env["BASE"] || "/";

export default defineUserConfig({
  base: "/bjwz-china/",

  locales: {
    "/": {
      lang: "en-US",
      title: "BJWZ-China",
      description: "iGEM 2023",
    },
  },

  theme,

  shouldPrefetch: false,
});
